#!/bin/bash

debug=${1:-"release"}
rebuild=${2:-"fast"}

# LOAD REQUIRED MODULES
module load intel-oneapi-compilers/2022.0.1-gcc-11.2.0
module load intel-oneapi-mpi/2021.5.0-intel-2021.5.0
module load netcdf-c/4.8.1-intel-oneapi-mpi-2021.5.0-intel-2021.5.0
module load netcdf-fortran/4.5.3-intel-oneapi-mpi-2021.5.0-intel-2021.5.0

# define compilers for MPI wrappers
export I_MPI_CC=icc
export I_MPI_CXX=icpc
export I_MPI_F77=ifort
export I_MPI_F90=ifort

# GET IOW ESM ROOT PATH
export IOW_ESM_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/../.."

# SET SYSTEM-SPECIFIC COMPILER OPTIONS AND PATHS
# compile mode: "PRODUCTION" or "DEBUG"
if [ $debug == "debug" ]; then
	export IOW_ESM_COMPILE_MODE="DEBUG"
elif [ $debug == "release" ]; then
	export IOW_ESM_COMPILE_MODE="PRODUCTION"
else
	echo "Compile mode is not specified correctly. Use debug or release"
	exit;
fi

# include paths
export IOW_ESM_NETCDFC_INCLUDE="/sw/spack-levante/netcdf-c-4.8.1-7dq6g2/include"
export IOW_ESM_NETCDFC_LIBRARY="/sw/spack-levante/netcdf-c-4.8.1-7dq6g2/lib"
export IOW_ESM_NETCDFF_INCLUDE="/sw/spack-levante/netcdf-fortran-4.5.3-r5r3ev/include"
export IOW_ESM_NETCDFF_LIBRARY="/sw/spack-levante/netcdf-fortran-4.5.3-r5r3ev/lib"

# executables
export IOW_ESM_MAKE="/usr/bin/make"
export IOW_ESM_FC="mpiifort"
export IOW_ESM_CC="mpiicc"
export IOW_ESM_LD="mpiifort"


if [ $debug == "debug" ]; then
	# compiler flags
	export IOW_ESM_CPPDEFS="-DOASIS_IOW_ESM -DIOW_ESM_DEBUG"

	export IOW_ESM_FFLAGS="-O0 -r8 -g -traceback -check all -fp-model precise -xHost -I${IOW_ESM_NETCDFF_INCLUDE}/"
	export IOW_ESM_CFLAGS="-O0 -g -traceback -fp-model precise -xHost -I${IOW_ESM_NETCDFC_INCLUDE}/"
	export IOW_ESM_LDFLAGS="-g -traceback -L${IOW_ESM_NETCDFC_LIBRARY} -L${IOW_ESM_NETCDFF_LIBRARY} -lnetcdf -lnetcdff -Wl,-rpath,${IOW_ESM_NETCDFC_LIBRARY},-rpath,${IOW_ESM_NETCDFF_LIBRARY}"
else
	# compiler flags
	export IOW_ESM_CPPDEFS="-DOASIS_IOW_ESM"
	
	export IOW_ESM_FFLAGS="-O3 -r8 -align -pad -static-intel -fp-model precise -qopt-zmm-usage=high -xCORE-AVX512 -I${IOW_ESM_NETCDFF_INCLUDE}/"
	export IOW_ESM_CFLAGS="-O3 -align -pad -static-intel -fp-model precise -qopt-zmm-usage=high -xCORE-AVX512 -I${IOW_ESM_NETCDFC_INCLUDE}/"
	export IOW_ESM_LDFLAGS="-L${IOW_ESM_NETCDFC_LIBRARY} -L${IOW_ESM_NETCDFF_LIBRARY} -lnetcdf -lnetcdff -Wl,-rpath,${IOW_ESM_NETCDFC_LIBRARY},-rpath,${IOW_ESM_NETCDFF_LIBRARY}"
fi

# MAKE CLEAN
if [ $rebuild == "rebuild" ]; then
	rm -r ${IOW_ESM_ROOT}/components/MOM5/exec/IOW_ESM_${IOW_ESM_COMPILE_MODE}
fi

# RUN BUILD COMMAND
cd ${IOW_ESM_ROOT}/components/MOM5/exp
./MOM_compile.csh
cd ${IOW_ESM_ROOT}/components/MOM5

