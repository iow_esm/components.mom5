#!/bin/bash

debug=${1:-"release"}
rebuild=${2:-"fast"}

# CHECK FOR HOST TO DISTINGUISH THE BUILD SYSTEMS
OSNAME=`cat /etc/os-release | grep NAME | head -n 1 | awk '{print $1}' | sed -e 's/NAME="//'`

# LOAD REQUIRED MODULES
if [[ $OSNAME == *"Rocky"* ]]; then
  module load intel/2024.2
  module load impi/2021.13
  module load netcdf/intel/4.9.2
else
  module load intel/18.0.6
  module load impi/2018.5
  module load netcdf/intel/4.7.3
fi

# GET IOW ESM ROOT PATH
export IOW_ESM_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/../.."

# SET SYSTEM-SPECIFIC COMPILER OPTIONS AND PATHS
# compile mode: "PRODUCTION" or "DEBUG"
if [ $debug == "debug" ]; then
	export IOW_ESM_COMPILE_MODE="DEBUG"
elif [ $debug == "release" ]; then
	export IOW_ESM_COMPILE_MODE="PRODUCTION"
else
	echo "Compile mode is not specified correctly. Use debug or release"
	exit;
fi

# include paths
if [[ $OSNAME == *"Rocky"* ]]; then
  export IOW_ESM_NETCDF_INCLUDE="/sw/dataformats/netcdf/4.9.2/clx.el9/intel/include"
  export IOW_ESM_NETCDF_LIBRARY="/sw/dataformats/netcdf/4.9.2/clx.el9/intel/lib"

  # executables
  export IOW_ESM_FC="mpiifort"
  export IOW_ESM_CC="mpiicx"
  export IOW_ESM_LD="mpiifort"
else
  export IOW_ESM_NETCDF_INCLUDE="/sw/dataformats/netcdf/intel.18/4.7.3/skl/include"
  export IOW_ESM_NETCDF_LIBRARY="/sw/dataformats/netcdf/intel.18/4.7.3/skl/lib"

  # executables
  export IOW_ESM_FC="mpiifort"
  export IOW_ESM_CC="mpiicc"
  export IOW_ESM_LD="mpiifort"

fi

# make executable
export IOW_ESM_MAKE="/usr/bin/make"


if [ $debug == "debug" ]; then
	# compiler flags
	export IOW_ESM_CPPDEFS="-DOASIS_IOW_ESM -DIOW_ESM_DEBUG"

	export IOW_ESM_FFLAGS="-O0 -r8 -g -traceback -check all -fp-model precise -xCORE-AVX512 -diag-disable=10448 -I${IOW_ESM_NETCDF_INCLUDE}/"
	export IOW_ESM_CFLAGS="-O0 -g -traceback -fp-model precise -xCORE-AVX512 -I${IOW_ESM_NETCDF_INCLUDE}/"
	export IOW_ESM_LDFLAGS="-g -traceback -L${IOW_ESM_NETCDF_LIBRARY} -lnetcdf -lnetcdff -Wl,-rpath,${IOW_ESM_NETCDF_LIBRARY}"
else
	# compiler flags
	export IOW_ESM_CPPDEFS="-DOASIS_IOW_ESM"
	
	export IOW_ESM_FFLAGS="-O3 -r8 -align -pad -static-intel -fp-model precise -qopt-zmm-usage=high -xCORE-AVX512 -diag-disable=10448 -I${IOW_ESM_NETCDF_INCLUDE}/"
	export IOW_ESM_CFLAGS="-O3 -align -pad -static-intel -fp-model precise -qopt-zmm-usage=high -xCORE-AVX512 -I${IOW_ESM_NETCDF_INCLUDE}/"
	export IOW_ESM_LDFLAGS="-L${IOW_ESM_NETCDF_LIBRARY} -lnetcdf -lnetcdff -Wl,-rpath,${IOW_ESM_NETCDF_LIBRARY}"
fi

# replace flags if compiling with icx
if [[ $OSNAME == *"Rocky"* ]]; then
  if [ $debug == "debug" ]; then
    export IOW_ESM_CFLAGS="-O0 -g -fp-model=precise -xHost -I${IOW_ESM_NETCDF_INCLUDE}/"
  else
    export IOW_ESM_CFLAGS="-qopt-dynamic-align -static-intel -fp-model=precise -qopt-zmm-usage=high -xHost -I${IOW_ESM_NETCDF_INCLUDE}/"
  fi
fi

# MAKE CLEAN
if [ $rebuild == "rebuild" ]; then
	rm -r ${IOW_ESM_ROOT}/components/MOM5/exec/IOW_ESM_${IOW_ESM_COMPILE_MODE}
fi

# RUN BUILD COMMAND
cd ${IOW_ESM_ROOT}/components/MOM5/exp
./MOM_compile.csh
cd ${IOW_ESM_ROOT}/components/MOM5

